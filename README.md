# Projeto fantasma ESTAT

## Nome
Projeto fantasma ESTAT - Bruno Gondim Toledo

Nome provisório do relatório: Análise de série histórica da copa do mundo

## Descrição
Repositório público de arquivos utilizados para a realização do projeto fantasma do projeto seletivo da ESTAT - Empresa júnior de estatística, de autoria de Bruno Gondim Toledo. 

## Software
O sistema operacional utilizado na elaboração de todos os arquivos deste repositório foi o Windows 11.
Para a utilização plena dos arquivos aqui contidos, é necessário ter ao menos instalado em sua máquina:
R (versão 4.2.1), Rstudio, algum navegador (para utilização do Overleaf), além de aplicativos padrão da Microsoft para visualização de arquivos .txt, .png, etc, ou outros equivalentes.
Não foi testado em outros sistemas operacionais. Não há como garantir o funcionamento completo ou parcial do projeto em outro SO que não o Windows 11. Não há como garantir o funcionamento completo ou parcial dos códigos R em outra IDE que não o Rstudio.

## Aplicação
Análises estatísticas e relatório acerca de dois bancos de dados sobre as copas do mundo. Os dados foram fornecidos pela equipe da ESTAT. O autor do projeto não se responsabiliza pelos dados. O autor do projeto não sabe se os dados são reais ou fictícios.

## Suporte
E-mails do autor:
Pessoal:      brunogtoledo96@gmail.com
Profissional: bruno.gondim@estat.com.br
Estudantil:   bruno.gondim@aluno.unb.br

Telefone/WhatsApp: +55 (61) 9 9544-6161

O autor não fornece garantia ou suporte acerca dos arquivos neste repositório.

## Contribuidores
A elaboração das análises estatísticas e do relatório presente neste documento é de inteira responsabilidade do autor. Entretando, foram utilizados/as padronizações, guias e direcionamentos fornecidos/as pela empresa ESTAT - Consultoria Estatística. A utilização dos materiais desta empresa contidas neste projeto seguem as políticas de privacidade da empresa.

## Licença
Propriedade particular-pública. A utilização, publicação e cópia dos arquivos contidos neste repositório manufaturadas pelo autor, é autorizada pelo autor de forma livre e ilimitada.
A utilização dos materiais da empresa ESTAT - Consultoria Estatística contidas neste projeto (padronizações, guias, direcionamentos, etc) seguem as políticas de privacidade da empresa.

## Etapas do projeto
O projeto consiste, basicamente, em 7 etapas:

1 - Início do projeto, ajuste dos bancos, leitura dos materiais, etc

2 - Primeiro item do projeto (com entrega parcial para os revisores da empresa)

3 - Segundo item do projeto (com entrega parcial para os revisores da empresa)

4 - Terceiro item do projeto (com entrega parcial para os revisores da empresa)

5 - Quarto item do projeto (com entrega parcial para os revisores da empresa)

6 - Quinto item do projeto (com entrega parcial para os revisores da empresa)


7 - Consolidação, revisão, entrega final do projeto.

## Status do projeto

1ª Etapa: Concluída

2ª Etapa: Concluída

3ª Etapa: Concluída

4ª Etapa: Concluída

5ª Etapa: Concluída

6ª Etapa: Concluída

7ª Etapa: Concluída

*Concluído*
